﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Cache;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DeadlockExample
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
		}

		private async void Button_Click(object sender, RoutedEventArgs e)
		{
			Action<float> func = new Action<float>(value => MainProgress.Value = value * 100);
			Action<float> uiSafeFunc = new Action<float>(value => Dispatcher.Invoke(() => func(value)));

			var content = await GetYandexAndGoogle();
			SingleButton.Content = (await content.First().Content.ReadAsStringAsync()).Substring(0, 50);

			//var content = GetFileContentAsync(func).Result;
			//func.Report(0.5f);
			//CallBadAsync().GetAwaiter().GetResult();
			/*var content = await GetYandexAsync(func)
				.ConfigureAwait(false);*/
			//MessageBox.Show(content.Substring(0, 100));
			//System.Diagnostics.Debug.WriteLine($"After all ThreadId: {Environment.CurrentManagedThreadId}");*/
		}

		private async Task<IEnumerable<HttpResponseMessage>> GetYandexAndGoogle()
		{
			using var httpClient = new HttpClient();
			Task<HttpResponseMessage> yandexTask = httpClient.GetAsync("http://yandex.ru");
			Task<HttpResponseMessage> googleTask = httpClient.GetAsync("http://google.com");

			return await Task.WhenAll(new[] { yandexTask, googleTask })
				.ConfigureAwait(false);
		}

		private async Task CallBadAsync()
		{
			// UI
			await Task.Delay(10)
				.ConfigureAwait(continueOnCapturedContext: false);
			// ?
			await Task.Delay(10)
				.ConfigureAwait(continueOnCapturedContext: true);
			//
		}

		public async Task<string> GetFileContentAsync(Action<float>? progressReporter = null)
		{
			using var httpClient = new HttpClient();

			Debug.WriteLine($"Before ThreadId: {Environment.CurrentManagedThreadId}");
			var response = await httpClient.GetAsync("https://file-examples.com/storage/fef4b1398b62456fdb0c0ff/2017/02/file_example_XML_24kb.xml",
				HttpCompletionOption.ResponseHeadersRead)
				.ConfigureAwait(false);
			Debug.WriteLine($"After ThreadId: {Environment.CurrentManagedThreadId}");
			response.EnsureSuccessStatusCode();
			var dataLength = response.Content.Headers.ContentLength;
			if (!dataLength.HasValue || progressReporter is null)
				return await response.Content
					.ReadAsStringAsync()
					.ConfigureAwait(false);

			var buffer = new byte[dataLength.Value];
			var readBytesCount = 0;
			using (var contentStream = await response.Content
				.ReadAsStreamAsync()
				.ConfigureAwait(false))
			{
				while (readBytesCount < dataLength)
				{
					readBytesCount += await contentStream.ReadAsync(buffer,
						offset: readBytesCount,
						count: (readBytesCount + 100) < (int)dataLength.Value
							? 100
							: (int)dataLength.Value - readBytesCount)
						.ConfigureAwait(false);
					await Task.Delay(1).ConfigureAwait(false);
					progressReporter?.Invoke((float)readBytesCount / dataLength.Value);
				}
			}
			return Encoding.UTF8.GetString(buffer);
		}
	}
}
