﻿using System;

namespace BookingPlatform.WebApi.Dto.V1
{
	public class UserLoginResponse
	{
		public string Token { get; }

		public UserLoginResponse(string token)
		{
			if (string.IsNullOrEmpty(token))
				throw new ArgumentException(nameof(token));
			Token = token;
		}
	}
}
