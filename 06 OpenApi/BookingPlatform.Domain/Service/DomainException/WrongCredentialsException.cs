﻿using System.Net;

namespace BookingPlatform.Domain.Service.DomainException
{
	public class WrongCredentialsException : StatusCodedException
	{
		public WrongCredentialsException(string message) : base(statusCode: (int)HttpStatusCode.Unauthorized, message)
		{
		}
	}
}
