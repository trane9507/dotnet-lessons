import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RoomClient } from '../../client/room-client';

@Component({
  selector: 'app-create-room-form',
  templateUrl: './create-room-form.component.html'
})
export class CreateRoomFormComponent implements OnInit {
  public nameControl = new FormControl('', Validators.min(5));
  public descriptionControl = new FormControl('', Validators.min(5));

  constructor(private roomClient: RoomClient,
    private router: Router) { }

  ngOnInit(): void {
  }

  isSubmitButtonDisabled() {
    return this.nameControl.invalid || this.descriptionControl.invalid;
  }

  submitForm() {
    this.roomClient.createRoom(this.nameControl.value, this.descriptionControl.value)
      .subscribe(r => this.router.navigateByUrl(''));
  }

}
