import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { FormControl, Validators } from "@angular/forms";
import { environment } from "src/environments/environment";
import { AuthService } from "../../auth/auth.service";

import { CreateRoomDto } from "./create-room-dto";
import { CreateRoomResponse } from "./create-room-response";

@Injectable({
  providedIn: 'root'
})
export class RoomClient {
  private readonly roomApiUrl = "/v2/room";

  constructor(private authService: AuthService,
    private httpClient: HttpClient) {
  }

  public createRoom(name: string, description: string) {
    if (this.authService.apiKey == '')
      throw new Error("User is not authenticated");
    let dto : CreateRoomDto =
    {
      roomName: name,
      description: description
    }
    return this.httpClient
      .post<CreateRoomResponse>(environment.bookingServerUrl + this.roomApiUrl, dto,
        )
  }
}
