export interface CreateRoomResponse {
  id: number,
  roomName: string
}
