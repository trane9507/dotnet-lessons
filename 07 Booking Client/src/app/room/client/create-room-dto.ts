export interface CreateRoomDto {
  roomName: string,
  description: string
}
