import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject, map } from 'rxjs';
import jwt_decode from 'jwt-decode';

import { UserLoginResponse } from './user-login-reponse'

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private loginAddress = "/v1/user/login";

  public apiKey: string;
  public redirectUrl: string;
  public loggedIn: Subject<string>;

  constructor(private httpClient: HttpClient) {
    this.apiKey = ''
    this.redirectUrl = ''
    this.loggedIn = new Subject<string>();
  }

  login(username: string, password: string) {
    const body = { username: username, password: password };

    return this.httpClient.post<UserLoginResponse>(environment.bookingServerUrl + this.loginAddress,
      body)
      .pipe(map(token => this.parseAndSaveToken(token)))
  }

  logOut() {
    this.apiKey = '';
    this.loggedIn.next('');
  }

  private parseAndSaveToken(userLoginResponse: UserLoginResponse) : string {
    this.apiKey = userLoginResponse.token;
    let decoded : any = jwt_decode(userLoginResponse.token);
    this.loggedIn.next(decoded.name);
    return decoded.name;
  }
}
