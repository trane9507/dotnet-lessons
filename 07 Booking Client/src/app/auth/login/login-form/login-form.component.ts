import { HttpErrorResponse, HttpStatusCode } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AuthService } from '../../auth.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {
  public hide = true;

  emailControl = new FormControl('', Validators.email);
  passwordControl = new FormControl('', Validators.min(5));

  constructor(private authService: AuthService,
    private snackBar: MatSnackBar,
    private router: Router) { }

  ngOnInit(): void {
    this.emailControl.setValue('user@email.com');
    this.passwordControl.setValue('password');
  }

  hideClicked() {
    this.snackBar.open('Credentials are wrong');
    this.hide = !this.hide;
  }

  isSubmitButtonDisabled() {
    return this.emailControl.invalid || this.passwordControl.invalid;
  }

  submitForm() {
    if (this.emailControl.invalid || this.passwordControl.invalid)
      return;
    this.authService.login(this.emailControl.value, this.passwordControl.value)
      .subscribe({
        next: username => {
          if (this.authService.redirectUrl != '')
            this.router.navigateByUrl(this.authService.redirectUrl)
          else
            this.router.navigateByUrl('');
        },
        error: err => {
          if (err.status == HttpStatusCode.Unauthorized) {
            this.snackBar.open('Credentials are wrong', 'Dismiss');
            console.error("Opened");
            return;
          }
          this.snackBar.open('Unknow errror, please contact the support', 'Dismiss');
        }
      });
  }
}
