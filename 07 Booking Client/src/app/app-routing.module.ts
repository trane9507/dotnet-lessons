import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardGuard } from './auth/auth-guard.guard';
import { LoginFormComponent } from './auth/login/login-form/login-form.component';
import { CreateBookingComponent } from './create-booking/create-booking.component';
import { CreateRoomFormComponent } from './room/create-room/create-room-form/create-room-form.component';

const routes: Routes = [
  {
    path: 'create-room',
    component: CreateRoomFormComponent,
    canActivate: [AuthGuardGuard]
  },
  {
    path: 'login',
    component: LoginFormComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
